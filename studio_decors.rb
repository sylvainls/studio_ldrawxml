#!/usr/bin/ruby
# frozen_string_literal: true

# reads:
# - LDD’s ldraw.xml file (named ldraw_ldd.xml)
# - decors_lxf2ldr.yaml file (named decors_lxf2io.yaml)
# - data at the end of the file
# outputs on STDOUT:
# - Decoration elements with a few comments

require 'nokogiri'
require 'yaml'
require 'matrix'

def rot2mat( ax, ay, az, angle )
  cos = Math.cos( angle )
  sin = Math.sin( angle )
  l = ax
  m = ay
  n = az
  ll = l**2
  mm = m**2
  nn = n**2
  lm = l * m
  nl = n * l
  nm = n * m
  c1 = 1 - cos
  Matrix[ [ ll * c1 + cos,     lm * c1 - n * sin, nl * c1 + m * sin ],
          [ lm * c1 + n * sin, mm * c1 + cos,     nm * c1 - l * sin ],
          [ nl * c1 - m * sin, nm * c1 + l * sin, nn * c1 + cos ] ]
end

EPS = 0.00001
EPS2 = 0.001
SR2 = Math.sqrt( 2 )
def mat2rot( m )
  m21_12 = m[2,1] - m[1,2]
  m02_20 = m[0,2] - m[2,0]
  m10_01 = m[1,0] - m[0,1]
  if m21_12.abs < EPS && m02_20.abs < EPS && m10_01.abs < EPS
    # singularity
    # ID?
    if ( m[1, 0] + m[0, 1] ).abs < EPS2 &&
       ( m[0, 2] + m[2, 0] ).abs < EPS2 &&
       ( m[2, 1] + m[1, 2] ).abs < EPS2 &&
       ( m[0, 0] + m[1, 1] + m[2, 2] - 3 ).abs < EPS2
      # id
      return { 'angle' => 0, 'ax' => 0, 'ay' => 1, 'az' => 0 }
    end

    # PI
    xx = ( m[0, 0] + 1.0 ) / 2.0
    yy = ( m[1, 1] + 1.0 ) / 2.0
    zz = ( m[2, 2] + 1.0 ) / 2.0
    xy = ( m[0, 1] + m[1, 0] ) / 4.0
    xz = ( m[0, 2] + m[2, 0] ) / 4.0
    yz = ( m[1, 2] + m[2, 1] ) / 4.0
    if xx > yy && xx > zz # m[0,0] is the largest diagonal term
      return { 'angle' => Math::PI, 'ax' => 0, 'ay' => SR2, 'az' => SR2 } if xx < EPS

      x = Math.sqrt( xx )
      return { 'angle' => Math::PI, 'ax' => x, 'ay' => xy / x, 'az' => xz / x }
    end
    if yy > zz # m[1,1] is the largest diagonal term
      return { 'angle' => Math::PI, 'ax' => SR2, 'ay' => 0, 'az' => SR2 } if yy < EPS

      y = Math.sqrt( yy )
      return { 'angle' => Math::PI, 'ax' => xy / y, 'ay' => y, 'az' => yz / y }
    end
    # m[2,2] is the largest diagonal term so base result on this
    return { 'angle' => Math::PI, 'ax' => SR2, 'ay' => SR2, 'az' => 0 } if zz < EPS

    z = Math.sqrt( zz )
    return { 'angle' => Math::PI, 'ax' => xz / z, 'ay' => yz / z, 'az' => z }
  end
  # normal case
  r = Math.sqrt( m21_12**2 + m02_20**2 + m10_01**2 )
  {
    'angle' => Math.acos( ( m[0, 0] + m[1, 1] + m[2, 2] - 1.0 ) / 2.0 ),
    'ax' => m21_12 / r,
    'ay' => m02_20 / r,
    'az' => m10_01 / r
  }
end

ATTRS_LDD = %w[ ldraw angle ax ay az tx ty tz ]
LDD_LDRAW2LEGO = {}
LDD_LEGO2LDRAW = {}
Nokogiri::XML::Document.parse( File.open( 'ldraw_ldd.xml' ) )
                       .xpath( '//Brick' ).each do |match|
  lego = match['lego']
  ldraw = match['ldraw']
  LDD_LDRAW2LEGO[ldraw] ||= []
  LDD_LDRAW2LEGO[ldraw] << lego
  LDD_LEGO2LDRAW[lego] ||= []
  LDD_LEGO2LDRAW[lego] << ldraw
end
LDD_TRANS = {}
Nokogiri::XML::Document.parse( File.open( 'ldraw_ldd.xml' ) )
                       .xpath( '//Transformation' ).each do |trans|
  info = {}
  ATTRS_LDD.each do |attr|
    info[attr] = trans[attr]
  end
  ldraw = info['ldraw']
  lego = ldraw.split( '.' ).first
  LDD_LEGO2LDRAW[lego] ||= [ ldraw ]
  legos = LDD_LDRAW2LEGO[ldraw] || [ lego ]
  legos.each do |lo|
    LDD_TRANS[lo] ||= {}
    LDD_TRANS[lo][ldraw] ||= []
    LDD_TRANS[lo][ldraw] << info
  end
end

DECORS = YAML.load_file( 'decors_lxf2io.yaml' )
ATTRS_L = %w[ angle ax ay az tx ty tz ]
PAT_ROT = {
  'x'   => rot2mat( 1, 0, 0, Math::PI / 2 ),
  'xx'  => rot2mat( 1, 0, 0, Math::PI ),
  'xxx' => rot2mat( 1, 0, 0, -Math::PI / 2 ),
  'y'   => rot2mat( 0, 1, 0, Math::PI / 2 ),
  'yy'  => rot2mat( 0, 1, 0, Math::PI ),
  'yyy' => rot2mat( 0, 1, 0, -Math::PI / 2 ),
  'z'   => rot2mat( 0, 0, 1, Math::PI / 2 ),
  'zz'  => rot2mat( 0, 0, 1, Math::PI ),
  'zzz' => rot2mat( 0, 0, 1, -Math::PI / 2 )
}
TOR_TAP = {
  'x'   => 'x90',
  'xx'  => 'x180',
  'xxx' => 'x270',
  'y'   => 'y90',
  'yy'  => 'y180',
  'yyy' => 'y270',
  'z'   => 'z90',
  'zz'  => 'z180',
  'zzz' => 'z270'
}
def attr_to_s( info, a )
  '%s="%s"' % [ a, info[a] ]
end

def float_to_s( v )
  v.abs < 0.0001 ? '0' : '%.7g' % v
end

def rot_to_s( rot )
  rot.map { |a, v| '%s="%s"' % [ a, float_to_s( v ) ] }.join( ' ' )
end

def pos_to_s( vec )
  'tx="%s" ty="%s" tz="%s"' % 3.times.map { |i| float_to_s( vec[i, 0] ) }
end

VARIANTS, RENAME, MISSING = YAML.load_stream( DATA.read )

DECORS.each do |lego, ph|
  next unless ph['decorations']

  _ldraw, ldds = LDD_TRANS[lego.to_s].to_a.last
  ldd = ldds.last
  ph['decorations'].each do |dec_field, file_rot|
    dec_ldraw, dec_rot = file_rot.split( ' ' )
    dec_ldraw = RENAME[dec_ldraw] if RENAME.key?( dec_ldraw )
    puts( '  <!-- .dat missing in Studio -->' ) if MISSING.key?( dec_ldraw )
    if dec_rot
      pat_rot = PAT_ROT[dec_rot]
      com_rot = TOR_TAP[dec_rot] || dec_rot
      if pat_rot.nil? # special rotation
        if ( m = /\A([xyz])(\d+)\Z/.match( dec_rot ) )
          axis = m[1]
          angle = m[2]
          pat_rot = rot2mat( axis == 'x' ? 1 : 0,
                             axis == 'y' ? 1 : 0,
                             axis == 'z' ? 1 : 0,
                             angle.to_f / 180 * Math::PI )
        end
      end
      ldd_rot = rot2mat( ldd['ax'].to_f, ldd['ay'].to_f, ldd['az'].to_f, ldd['angle'].to_f )
      final_rot = mat2rot( pat_rot * ldd_rot )
      final_rot['angle'] = -final_rot['angle'] unless final_rot['angle'].zero? || final_rot['angle'] == Math::PI
      final_pos = pat_rot * Matrix.column_vector( [ ldd['tx'].to_f, ldd['ty'].to_f, ldd['tz'].to_f ] )
      puts( '  <!-- Rotated %s -->' % com_rot )
      puts( '  <Decoration ldraw="%s" lego="%s" decoration="%s" type="" %s %s />' %
            [ dec_ldraw, lego, dec_field, rot_to_s( final_rot ), pos_to_s( final_pos ) ] )
      if VARIANTS.key?( dec_ldraw )
        VARIANTS[dec_ldraw].each do |var_ldraw|
          puts( '  <!-- Rotated %s -->' % com_rot )
          puts( '  <Decoration ldraw="%s" lego="%s" decoration="%s" type="to_lego" %s %s />' %
                [ var_ldraw, lego, dec_field, rot_to_s( final_rot ), pos_to_s( final_pos ) ] )
        end
      end
    else
      puts( '  <Decoration ldraw="%s" lego="%s" decoration="%s" type="" %s />' %
            [ dec_ldraw, lego, dec_field, ATTRS_L.map { |a| attr_to_s( ldd, a ) }.join( ' ' ) ] )
      if VARIANTS.key?( dec_ldraw )
        VARIANTS[dec_ldraw].each do |var_ldraw|
          puts( '  <Decoration ldraw="%s" lego="%s" decoration="%s" type="to_lego" %s />' %
                [ var_ldraw, lego, dec_field, ATTRS_L.map { |a| attr_to_s( ldd, a ) }.join( ' ' ) ] )
        end
      end
    end
  end
end

__END__
--- # variants
3626cp88.dat: [ 3626bp88.dat ]
3626cp8a.dat: [ 3626bp8a.dat ]
3626cp8c.dat: [ 3626bp8c.dat ]

--- # LDraw / Studio
10164p01.dat: 10164pb01.dat
14769p0f.dat: 14769pb001.dat
18835p01.dat: 18835pb01.dat
19729p00.dat: 19729pb005.dat
19729p03.dat: 19729pb003.dat
19729p05.dat: 19729pb004.dat
19729p07.dat: 19729pb008.dat
20695p01.dat: 20695pb01.dat
2431p04.dat: 2431p52.dat
2528ap33.dat: 2528pb06.dat
25866p01.dat: 25866c01.dat
25866p02.dat: 25866c02.dat
30152ap01.dat: 30152c01.dat
30152ap02.dat: 30152c02.dat
3068bpc0.dat: 3068bpb0311.dat
3069bpc9.dat: 3069bpb0030.dat
3815bp8i.dat: 3815p8i.dat
3815bpc44.dat: 3815pc44.dat
3815bpc67.dat: 3815pc67.dat
3815bpc6a.dat: 3815pc6a.dat
3816bpc6a.dat: 3816pc6a.dat
3817bpc6a.dat: 3817pc6a.dat
44375aps3.dat: 44375p03.dat
57028p01.dat: 57028a.dat
57028p02.dat: 57028b.dat
57028p03.dat: 57028c.dat
57028p04.dat: 57028d.dat
61406p06.dat: 61406pb06.dat
61406p07.dat: 61406pb07.dat
87610ps0.dat: 87610pb01.dat
87610ps1.dat: 87610pb02.dat
87610ps2.dat: 87610pb03.dat
87610ps3.dat: 87610pb04.dat
87610ps4.dat: 87610pb05.dat
87610ps6.dat: 87610pb07.dat
87610ps7.dat: 87610pb08.dat
87990p01.dat: 87990pb01.dat
87990p02.dat: 87990pb02.dat
90370p03.dat: 90370pb03.dat
90370p04.dat: 90370pb04.dat
90370p05.dat: 90370pb05.dat
93220p01.dat: 93220pb01.dat
93220p02.dat: 93220pb03.dat
973p9u.dat: 973pb0580.dat
973p9w.dat: 973pb0639.dat
973p9y.dat: 973pb0322.dat
973pf0.dat: 973pb0419.dat
973pf1.dat: 973pb0438.dat

--- # missing
11467p01.dat: 11467p01.dat
12895p01.dat: 12895p01.dat
12895p02.dat: 12895p02.dat
12895p03.dat: 12895p03.dat
12895p04.dat: 12895p04.dat
15553p01.dat: 15553p01.dat
15851p01.dat: 15851p01.dat
16000p05.dat: 16000p05.dat
16001p05.dat: 16001p05.dat
19727p00.dat: 19727p00.dat
19727p03.dat: 19727p03.dat
20460p00.dat: 20460p00.dat
20460p01.dat: 20460p01.dat
20460p02.dat: 20460p02.dat
20460p03.dat: 20460p03.dat
20460p04.dat: 20460p04.dat
20460p05.dat: 20460p05.dat
20460p06.dat: 20460p06.dat
20460p07.dat: 20460p07.dat
20460p08.dat: 20460p08.dat
20460p09.dat: 20460p09.dat
20460px0.dat: 20460px0.dat
20461p00.dat: 20461p00.dat
20461p01.dat: 20461p01.dat
20461p02.dat: 20461p02.dat
20461p03.dat: 20461p03.dat
20461p04.dat: 20461p04.dat
20461p05.dat: 20461p05.dat
20461p06.dat: 20461p06.dat
20461p07.dat: 20461p07.dat
20461p08.dat: 20461p08.dat
20461p09.dat: 20461p09.dat
20461px0.dat: 20461px0.dat
21700p01.dat: 21700p01.dat
23816c01.dat: 23816c01.dat
23816c02.dat: 23816c02.dat
25126p03.dat: 25126p03.dat
25866p03.dat: 25866p03.dat
30527c01.dat: 30527c01.dat
30527c02.dat: 30527c02.dat
30527c03.dat: 30527c03.dat
3899p01.dat: 3899p01.dat
47899c01.dat: 47899c01.dat
47899c03.dat: 47899c03.dat
47899c04.dat: 47899c04.dat
73194c01.dat: 73194c01.dat
73194c02.dat: 73194c02.dat
73194c03.dat: 73194c03.dat
73194c04.dat: 73194c04.dat
92709c01.dat: 92709c01.dat
92709c02.dat: 92709c02.dat
92709c03.dat: 92709c03.dat
92709c04.dat: 92709c04.dat
93230p01.dat: 93230p01.dat
93230p02.dat: 93230p02.dat
93230p03.dat: 93230p03.dat
93230p04.dat: 93230p04.dat
98570p01.dat: 98570p01.dat
99244p01.dat: 99244p01.dat
99244p02.dat: 99244p02.dat
3069bpc0.dat: 3069bpc0.dat
2586pc39.dat: 2586pc39.dat
19729p08.dat: 19729p08.dat
