#!/usr/bin/ruby
# frozen_string_literal: true

# reads:
# - LDD’s ldraw.xml file (named ldraw_ldd.xml)
# outputs on STDOUT:
# - the Transformation elements for Studio’s ldraw.xml

require 'nokogiri'
require 'yaml'

ATTRS_LDD = %w[ ldraw angle ax ay az tx ty tz ]
LDD_LDRAW2LEGO = {}
LDD_LEGO2LDRAW = {}
Nokogiri::XML::Document.parse( File.open( 'ldraw_ldd.xml' ) )
                       .xpath( '//Brick' ).each do |match|
  lego = match['lego']
  ldraw = match['ldraw']
  LDD_LDRAW2LEGO[ldraw] ||= []
  LDD_LDRAW2LEGO[ldraw] << lego
  LDD_LEGO2LDRAW[lego] ||= []
  LDD_LEGO2LDRAW[lego] << ldraw
end
LDD_TRANS = {}
Nokogiri::XML::Document.parse( File.open( 'ldraw_ldd.xml' ) )
                       .xpath( '//Transformation' ).each do |trans|
  info = {}
  ATTRS_LDD.each do |attr|
    info[attr] = trans[attr]
  end
  ldraw = info['ldraw']
  lego = ldraw.split( '.' ).first
  LDD_LEGO2LDRAW[lego] ||= [ ldraw ]
  LDD_LDRAW2LEGO[ldraw] ||= [ lego ]
  LDD_LDRAW2LEGO[ldraw].each do |lo|
    LDD_TRANS[lo] ||= {}
    LDD_TRANS[lo][ldraw] ||= []
    LDD_TRANS[lo][ldraw] << info
  end
end

ATTRS_L = %w[ angle ax ay az tx ty tz ]
def output( lego, ldraw, trans, type )
  '  <Transformation ldraw="%s" lego="%s" type="%s" %s />' %
    [ ldraw, lego, type, ATTRS_L.map { |a| '%s="%s"' % [ a, trans[a] ] }.join( ' ' ) ]
end

# ldraw lego -> type
#
# unique match:
#   a    b   -> ""     # same as multiple ldraw
#
# multiple ldraw:
#   a    b   -> "to"
#   a'   b   -> "to"
#   a"   b   -> ""     # = last of ldraws
#
# multiple lego:
#   a    b   -> "from" # = not last of legos
#   a    b'  -> ""
LDD_TRANS.each do |lego, ldraws|
  last = ldraws.size - 1
  ldraws.each.with_index do |ldrawtrans, i|
    ldraw, trans = ldrawtrans
    type = ''
    type = 'to_lego' if i < last
    legos = LDD_LDRAW2LEGO[ldraw]
    warn( 'NO_MATCH %s' % [ ldraw ] ) if legos.nil?
    type = 'from_lego' if legos.index( lego ) < legos.size - 1
    # if there’s multiples transformations, it’s because of new custom parts
    # overwriting the part previously used as a variant
    puts( output( lego, ldraw, trans.last, type ) )
  end
end
