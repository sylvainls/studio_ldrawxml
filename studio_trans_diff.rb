#!/usr/bin/ruby
# frozen_string_literal: true

# reads:
# - LDD’s ldraw.xml file (named ldraw_ldd.xml)
# - Studio’s ldraw.xml file (named ldraw_studio_orig.xml)
# - data at the end of the file
# outputs on STDOUT:
# - the corrected version of Studio’s ldraw.xml file

require 'nokogiri'
require 'yaml'

ATTRS_LDD = %w[ ldraw angle ax ay az tx ty tz ]
LDD_LDRAW2LEGO = {}
LDD_LEGO2LDRAW = {}
Nokogiri::XML::Document.parse( File.open( 'ldraw_ldd.xml' ) )
                       .xpath( '//Brick' ).each do |match|
  lego = match['lego']
  ldraw = match['ldraw']
  LDD_LDRAW2LEGO[ldraw] ||= []
  LDD_LDRAW2LEGO[ldraw] << lego
  LDD_LEGO2LDRAW[lego] ||= []
  LDD_LEGO2LDRAW[lego] << ldraw
end
LDD_TRANS = {}
Nokogiri::XML::Document.parse( File.open( 'ldraw_ldd.xml' ) )
                       .xpath( '//Transformation' ).each do |trans|
  info = {}
  ATTRS_LDD.each do |attr|
    info[attr] = trans[attr]
  end
  ldraw = info['ldraw']
  lego = ldraw.split( '.' ).first
  LDD_LEGO2LDRAW[lego] ||= [ ldraw ]
  legos = LDD_LDRAW2LEGO[ldraw] || [ lego ]
  legos.each do |lo|
    LDD_TRANS[lo] ||= {}
    LDD_TRANS[lo][ldraw] ||= []
    LDD_TRANS[lo][ldraw] << info
  end
end

ATTRS_L = %w[ angle ax ay az tx ty tz ]
ATTRS_S = %w[ type ]
def output( lego, ldraw, stu, ldd )
  '  <Transformation ldraw="%s" lego="%s" %s%s />' %
    [ ldraw, lego,
      ATTRS_S.map { |a| stu.key?( a ) ? '%s="%s" ' % [ a, stu[a] ] : '' }.join,
      ATTRS_L.map { |a| '%s="%s"' % [ a, ldd[a] ] }.join( ' ' ) ]
end

LOC = %w[ tx ty tz ]
ANG = %w[ angle ax ay az ]
ATTRS_STU = %w[ ldraw angle ax ay az tx ty tz type decoration lego ]
IDS_NOTES = YAML.safe_load( DATA.read )

better_match = {
  '13591' => '30088.dat',
  '23306' => '23306.dat',
  '92086' => '2635.dat',
  '10310' => '4488.dat',
  '15647' => '30390.dat',
  '15967' => '6060.dat',
  '19540' => '11953.dat',
  '19798' => '3940.dat',
  '20086' => '33172.dat',
  '21599' => '44375b.dat',
  '22412' => '59146.dat',
  '58856' => '76263.dat',
  '61356' => '71582.dat',
  '62575' => '23714.dat',
  '63140' => '572c02.dat',
  '64567' => '64567a.dat',
  '74968' => '87544.dat',
  '85532' => '71192.dat',
  '87587' => '2548.dat',
  '87753' => '92290.dat',
  '95345' => '4496.dat',
  '97927' => '61482.dat',
  '98011' => '30381.dat'
}

File.readlines( ARGV[0] || 'ldraw_studio_orig.xml' ).each do |line|
  if ( m = %r{\A\s*<Transformation (([^=]+="[^"]*" *)+) */>}.match( line ) )
    attrs = m[1]
    stu = {}
    attrs.scan( /([^=]+)="([^"]*)"/ ).each do |at, val|
      stu[at.strip] = val
    end
    lego = stu['lego']
    ldraw = stu['ldraw']
    ldd_info = LDD_TRANS[lego]
    if ldd_info.nil?
      puts( '  <!-- This lego ID is not in LDD -->' )
      puts( line )
      next
    end
    if ldd_info.key?( ldraw )
      ldd_info[ldraw].each do |ldd|
        if LOC.any? { |a| ldd[a] != stu[a] }
          puts( '  <!-- New translation -->' )
          puts( line.gsub( '<', '<!-- ' ).gsub( '>', ' -->' ) )
          puts( output( lego, ldraw, stu, ldd ) )
          next
        end
        if ( ldd['angle'] == '0' && stu['angle'] == '0' ) || ANG.all? { |a| ldd[a] == stu[a] }
          # Angle not changed
          puts( line )
          next
        end
        if ldd['angle'].to_f == -stu['angle'].to_f &&
           ( ( ldd['ax'].to_f == -stu['ax'].to_f && ldd['ay'] == stu['ay'] && ldd['az'] == stu['az'] ) ||
             ( ldd['ax'] == stu['ax'] && ldd['ay'].to_f == -stu['ay'].to_f && ldd['az'] == stu['az'] ) ||
             ( ldd['ax'] == stu['ax'] && ldd['ay'] == stu['ay'] && ldd['az'].to_f == -stu['az'].to_f ) )
          # Angle not changed
          puts( line )
          next
        end
        puts( '  <!-- New rotation -->' )
        puts( line.gsub( '<', '<!--' ).gsub( '>', '-->' ) )
        puts( output( lego, ldraw, stu, ldd ) )
      end
    else
      # Transformations I don’t know
      if IDS_NOTES[lego] && IDS_NOTES[lego][ldraw]
        puts( '  <!-- %s -->' % IDS_NOTES[lego][ldraw] )
        puts( line )
      elsif better_match[lego]
        puts( '  <!-- Better match -->' )
        puts( line.gsub( '<', '<!--' ).gsub( '>', '-->' ) )
        puts( output( lego, better_match[lego], stu, ldd_info[better_match[lego]].first ) )
      else
        puts( line )
      end
    end
  else
    puts( line )
  end
end

__END__
'11055':
  2335.dat: not the same variant (both in LDD)
'10190':
  2599.dat: not the same variant (both in LDD)
'3942':
  3942c.dat: not the same variant (both in LDD)
'3957':
  3957.dat: not the same variant (both in LDD)
'44375':
  44375b.dat: not the same variant (both in LDD)
'90194':
  48183.dat: not the same variant (both in LDD)
'58181':
  3939.dat: not the same variant (both in LDD)
'61252':
  6019.dat: not the same variant (both in LDD)
'28964':
  76766.dat: not the same variant (both in LDD)
'42611':
  92409.dat: not the same variant (both in LDD)
'92410':
  4532.dat: not the same variant (both in LDD)
'93106':
  4479a.dat: not the same variant (both in LDD)
'93348':
  4858.dat: not the same variant (both in LDD)
'95820':
  30237.dat: not the same variant (both in LDD)
'30038':
  30243.dat: dat does not exist
'18624':
  18624.dat: dat does not exist
'57028':
  57028.dat: dat does not exist
'60797':
  60797.dat: dat does not exist
'71372':
  71372.dat: dat does not exist
'10040':
  10040.dat: dat does not exist
'10089':
  10089.dat: dat does not exist
'10130':
  10130.dat: dat does not exist
'30085':
  x98.dat: dat does not exist
'54654':
  54654.dat: dat does not exist
'54923':
  54923.dat: dat does not exist
'59278':
  59278.dat: dat does not exist
'60410':
  x933c01.dat: dat does not exist
'9044':
  69.dat: incomplete model
'45406':
  45406.dat: incomplete model
'50859':
  50859.dat: incomplete model
'57029':
  57029.dat: incomplete model
'20693':
  20693.dat: incomplete model
'2533':
  2533.dat: incomplete model
'30527':
  30527.dat: incomplete model
'32052':
  32052.dat: incomplete model
'3750':
  3750.dat: incomplete model
'4493':
  4493.dat: incomplete model
'55205':
  55205.dat: incomplete model
'58124':
  58124.dat: incomplete model
'58135':
  58135.dat: incomplete model
'58148':
  58148.dat: incomplete model
'59143':
  59143.dat: incomplete model
'59154':
  59154.dat: incomplete model
'61521':
  61521.dat: incomplete model
'62271':
  62271.dat: incomplete model
'64415':
  64415.dat: incomplete model
'64424':
  64424.dat: incomplete model
'64778':
  64778.dat: incomplete model
'64780':
  64780.dat: incomplete model
'91968':
  91968.dat: incomplete model
'92694':
  92694.dat: incomplete model
'11399':
   66252.dat: wrong match
'31493':
   32064a.dat: wrong match
'3839':
   3839a.dat: wrong match
'3852':
   3852.dat: wrong match
'4518':
   4518a.dat: wrong match
'4719':
   4719c01.dat: wrong match
'54715':
   53787.dat: wrong match
'55967':
   53793.dat: wrong match
'53992':
   53992c01.dat: wrong match
'6126':
   28618.dat: wrong match
   6126b.dat: wrong match
'6542':
   6542.dat: wrong match
'42450':
   42450.dat: wrong match
'600880':
   600880.dat: wrong match
'62698':
   62698.dat: wrong match
'64417':
   64417.dat: wrong match
